local bump = require "lib.bump"
local sti = require "lib.sti"
local Camera = require "lib.camera"
local Player = require "player"

local lg = love.graphics

local game = {}

local bindings = {
  left = "left",
  right = "right",
  up = "jump"
}
local world
local map
local player
local camera

function game:enter()
  world = bump.newWorld()
  map = sti("maps/proto.lua", {"bump"})
  map:bump_init(world)
  player = Player(world, 0, 0)
  camera = Camera(player.x, player.y)
end

function game:update(dt)
  player:update(dt)
  map:update(dt)
  camera:lookAt(player.x, player.y)
end

function game:draw()
  camera:attach()
  local camX, camY = camera:worldCoords(0, 0)
  map:setDrawRange(camX, camY, lg.getWidth(), lg.getHeight())
  map:draw()
  player:draw()
  camera:detach()
end

function game:keypressed(key, scancode, isRepeat)
  if key == "r" then
    player.x, player.y, player.vx, player.vy = 0, 0, 0, 0
  elseif bindings[key] then
    player:keypressed(bindings[key])
  end
end

return game
