local utils = {}

function utils.cap(num, min, max)
  if num < min then
    return min
  elseif num > max then
    return max
  else return num
  end
end

function utils.rangeCap(num, range)
  return utils.cap(num, -range, range)
end

function utils.sign(num)
  if num > 0 then
    return 1
  elseif num < 0 then
    return -1
  else return 0
  end
end

return utils
