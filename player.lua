local Class = require "lib.class"
local vec = require "lib.vector-light"
local utils = require "utils"

local lg = love.graphics
local lk = love.keyboard

local Player = Class{}

local grav = 600
local xaccel = 400
local xdeccel = 300
local maxSpeed = 200

function Player:init(world, x, y)
  self.world = world
  self.x = x
  self.y = y
  self.w = 25
  self.h = 50
  self.vx = 0
  self.vy = 0
  self.dir = 0
  self.canJump = true

  world:add(self, self.x, self.y, self.w, self.h)
end

function Player:move(x, y)
  local cols, len
  self.x, self.y, cols, len = self.world:move(self, x, y)
  return cols, len
end

function Player:update(dt)
  if lk.isDown("left") then
    if self.vx > -maxSpeed then
      self.vx = self.vx - xaccel*dt
      if self.vx < -maxSpeed then
        self.vx = -maxSpeed
      end
    end
  elseif lk.isDown("right") then
    if self.vx < maxSpeed then
      self.vx = self.vx + xaccel*dt
      if self.vx > maxSpeed then
        self.vx = maxSpeed
      end
    end
  else
    if self.vx < 0 then
      self.vx = math.min(self.vx + xdeccel*dt, 0)
    else
      self.vx = math.max(self.vx - xdeccel*dt, 0)
    end
  end

  self.vy = self.vy + grav*dt

  local cols, len = self:move(self.x + self.vx*dt, self.y + self.vy*dt)
  for i = 1, len do
    local col = cols[i]
    if col.other.properties.isWall then
      if col.normal.x == 0 then
        self.vy = 0
        self.canJump = true
      else -- col.normal.y == 0
        self.vx = 0
      end
    end
  end
end

function Player:draw()
  lg.setColor(0, 255, 0, 70)
  lg.rectangle("fill", self.x, self.y, self.w, self.h)
  lg.setColor(0, 255, 0)
  lg.rectangle("line", self.x, self.y, self.w, self.h)
end

function Player:keypressed(key)
  if key == "right" then
    self.dir = 1
  elseif key == "left" then
    self.dir = -1
  elseif key == "jump" and self.canJump then
    self.vy = -400
    self.canJump = false
  end
end

return Player
