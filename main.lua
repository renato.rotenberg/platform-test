local Gamestate = require "lib.gamestate"
local game = require "game"

function love.load()
  Gamestate.switch(game)
end

function love.update(dt)
  Gamestate.update(dt)
end

function love.draw()
  Gamestate.draw()
end

function love.keypressed(key, scancode, isRepeat)
  Gamestate.keypressed(key, scancode, isRepeat)
end
